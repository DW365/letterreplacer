﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VistaKeysExtender.WinApi;

namespace LetterReplacer
{
    struct Pair
    {
        public char first;
        public char second;
        public Pair(char a, char b)
        {
            first = a;
            second = b;
        }
    }
    public partial class Form1 : Form
    {
        List<Pair> data;
        Random rand;
        public Form1()
        {
            var hot = new HotKey();
            hot.KeyModifier = HotKey.KeyModifiers.Control;
            hot.Key = Keys.Q;
            hot.HotKeyPressed += hot_HotKeyPressed;
            data = new List<Pair>();
            InitializeComponent();
            rand = new Random();
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(@"config.ini", System.Text.Encoding.Unicode);
            while ((line = file.ReadLine()) != null)
            {
                data.Add(new Pair(line.Split(':')[0][0],line.Split(':')[1][0]));
            }
        }

        void hot_HotKeyPressed(object sender, KeyEventArgs e)
        {
            richTextBox1.Text = "";
            Parse();
            Clipboard.SetText(richTextBox1.Text);
        }
        bool Rand(int x)
        {
            if (rand.Next(1, 101) <= x) return true;
            return false;
        }
        void AppendText(char chr)
        {
            var color = Color.Black;
            foreach(var pair in data)
            {
                if (chr == pair.second) color = Color.Red;
            }
            var text = Convert.ToString(chr);
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.SelectionLength = 0;
            richTextBox1.SelectionColor = color;
            richTextBox1.AppendText(text);
            richTextBox1.SelectionColor = richTextBox1.ForeColor;
        }
        void Parse()
        {
            string text = Clipboard.GetText();
            while(text.Length != 0)
            {
                var ch = text[0];
                text = text.Remove(0, 1);
                char ?r = null;
                foreach(var pair in data)
                {
                    if(ch == pair.first)
                    {
                        if(Rand(50))
                        {
                            r = pair.second;
                            break;
                        }
                    }
                    if (ch == pair.second)
                    {
                        if (Rand(50))
                        {
                            r = pair.first;
                            break;
                        }
                    }
                }
                if (r != null) AppendText(r.Value);
                else AppendText(ch);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
            Parse();
            Clipboard.SetText(richTextBox1.Text);
        }
    }
}
